#!/bin/env python3

# Copyright (C) 2020-2021 Nikola Hadžić
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the ""Software""), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED *AS IS*, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import gettext
import argparse
import sys
import statistics
import os
from PIL import Image

# Text domain variables.
TEXTDOMAIN = None
TEXTDOMAINDIR = None

# Set up internationalization environment.
_ = gettext.gettext
gettext.bindtextdomain(TEXTDOMAIN, TEXTDOMAINDIR)
gettext.textdomain(TEXTDOMAIN)

# Program name.
PROG = os.path.basename(sys.argv[0])

# Transform array maps pixel intensity to a character.
transform_array =   ('@' * 8) + \
                    ('W' * 8) + \
                    ('#' * 8) + \
                    ('&' * 8) + \
                    ('8' * 8) + \
                    ('B' * 8) + \
                    ('9' * 8) + \
                    ('S' * 8) + \
                    ('G' * 8) + \
                    ('H' * 8) + \
                    ('M' * 8) + \
                    ('h' * 8) + \
                    ('3' * 8) + \
                    ('5' * 8) + \
                    ('2' * 8) + \
                    ('A' * 8) + \
                    ('X' * 8) + \
                    ('Z' * 8) + \
                    ('k' * 8) + \
                    ('m' * 8) + \
                    ('w' * 8) + \
                    ('e' * 8) + \
                    ('s' * 8) + \
                    ('n' * 8) + \
                    ('r' * 8) + \
                    ('i' * 8) + \
                    (';' * 8) + \
                    (':' * 8) + \
                    ('o' * 8) + \
                    (',' * 8) + \
                    ('.' * 8) + \
                    (' ' * 8)

# Parses passed command-line arguments and returns the Namespace array which contains the parsed arguments (along with their values).
def argument_parser(argv):
    # Parser object. Also specify program name, description and epilog for the help menu.
    parser = argparse.ArgumentParser(prog = PROG, description = _("Image to ASCII art conversion tool."), epilog = "", add_help = True)

    # Describe all allowed command-line arguments.
    parser.add_argument("-v", "--version", action = "version", default = False, help = _("display version and exit"), dest = "version", version = "1.0")                # Display program version and exit.
    parser.add_argument("-o", "--overwrite", action = "store_true", default = False, help = _("overwrite output file"), dest = "overwrite")                             # Overwrite the output file without prompting.
    parser.add_argument("-c", "--columns", action = "store", default = 0, type = int, help = _("width of the ASCII art"), metavar = _("COLUMNS"), dest = "columns")     # Number of text columns.
    parser.add_argument("-r", "--rows", action = "store", default = 0, type = int, help = _("height of the ASCII art"), metavar = _("ROWS"), dest = "rows")             # Number of text rows.
    parser.add_argument("input", action = "store", type = str, help = _("input file name"), metavar = _("INPUT"))                                                       # Input file.
    parser.add_argument("output", action = "store", nargs = '?', default = None, type = str, help = _("output file name"), metavar = _("OUTPUT"))                       # Output file. If not supplied, STDOUT is used.

    # Parse given command-line arguments.
    arguments = parser.parse_args(argv)

    # Check for valid arguments.
    if arguments.columns < 0:
        parser.exit(_("%(prog)s: error: argument -c/--columns: the value must not be negative") % {"prog" : PROG})
    if arguments.rows < 0:
        parser.exit(_("%(prog)s: error: argument -r/--rows: the value must not be negative") % {"prog" : PROG})

    return arguments

# Attempts to open the input file for reading. In case of success the image is converted to ASCII text and the text is saved to the output file.
# If the output file is specified and already exists and "overwrite" flag is not given, the user is first asked whether they want to overwrite the file.
# In case of error, the program immediately exits with exit code 1.
def convert_image(filename_input, filename_output, overwrite, width, height):
    # Ask for confirmation to truncate the file if it already exists and "overwrite" is not given.
    if filename_output != None and not overwrite and os.path.exists(os.path.normpath(filename_output)):
        try:
            choice = input(_("Output file already exists. Overwrite it? [y/N] "))
        except EOFError:
            print('\n' + _("Invalid response."), file = sys.stderr)
            exit(1)
        else:
            if not (choice == _('y') or choice == _('Y')):
                exit(0)

    # Open the input file.
    try:
        image = Image.open(filename_input, mode = 'r')
    except FileNotFoundError:
        print(_("No such input file."), file = sys.stderr)
        exit(1)
    except IOError:
        print(_("Input file is not a valid image."), file = sys.stderr)
        exit(1)

    # Check for valid image size.
    if width > image.size[0]:
        print(_("Specified width is too big."), file = sys.stderr)
        exit(1)
    elif height > image.size[1]:
        print(_("Specified height is too big."), file = sys.stderr)
        exit(1)

    # If user specified 0 for width / height, set output width / height to image width / height, respectively.
    if width == 0:
            width = image.width
    if height == 0:
            height = image.height

    # Convert image to grayscale to get intensity of each pixel.
    image = image.convert(mode = 'L')

    # Create image of requested dimensions in memory.
    image_array = list()
    for y in range(0, image.height, image.height // height):
        rows = list()
        for r in range(0, min(image.height // height, image.height - y)):
            row = list()
            for x in range(0, image.width, image.width // width):
                col = list()
                for c in range(0, min(image.width // width, image.width - x)):
                    col.append(image.getpixel((x + c, y + r)))
                row.append(round(statistics.mean(col))) # Condense columns into one column.
            rows.append(row)
        # Condense rows into one row.
        row = list()
        for c in range(0, len(rows[0])):
            col = list()
            for r in rows:
                col.append(r[c])
            row.append(round(statistics.mean(col)))
        image_array.append(row)

    # Get the object representing the output file.
    try:
        output_file = open(filename_output, mode = 'w') if filename_output != None else sys.stdout
    except IOError:
        print(_("Failed to open output file."), file = sys.stderr)
        exit(1)

    # Convert each pixel in the image array to character that corresponds to it by intensity.
    # Save that character to the output file.
    for row in image_array:
        for col in row:
            print(transform_array[col], file = output_file, end = "")
        print(file = output_file)

    # Close the original image.
    image.close()

    # Close output file.
    if filename_output != None:
        output_file.close();

###  BEGINNING OF THE PROGRAM    ###
###==============================###

# First we parse the command-line arguments.
arguments = argument_parser(sys.argv[1:])

# Convert image to ASCII and save the conversion result to the output file or the screen.
convert_image(arguments.input, arguments.output, arguments.overwrite, arguments.columns, arguments.rows)
