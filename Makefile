# INSTALL COMMANDS
INSTALL := install
INSTALL_PROGRAM := $(INSTALL)
INSTALL_DATA := $(INSTALL) -m 644

# VARIABLES
PROG := image2ascii
INSTALLDIR := /usr/bin
LOCALEDIR := /usr/share/locale
LICENSEDIR := /usr/share/licenses

# FUNCTIONS

# Generate MO file from PO file into $(LOCALEDIR) given language code as $(1).
generate_mo = mkdir -p '$(LOCALEDIR)/$(1)/LC_MESSAGES' && msgfmt -o '$(LOCALEDIR)/$(1)/LC_MESSAGES/$(PROG).mo' 'po/$(1).po'

# Delete MO file from $(LOCALEDIR) given language code as $(1).
# Then delete all of its parent directories in bottom-to-top order if they are empty.
delete_mo = if test -d '$(LOCALEDIR)/$(1)/LC_MESSAGES'; then rm -f '$(LOCALEDIR)/$(1)/LC_MESSAGES/$(PROG).mo' && rmdir -p --ignore-fail-on-non-empty '$(LOCALEDIR)/$(1)/LC_MESSAGES'; fi

# RULES
# =====

.PHONY: help install uninstall

help:
	$(info VARIABLES)
	$(info ================================================================================)
	$(info PROG:       Name of the program as installed.)
	$(info INSTALLDIR: Installation directory.)
	$(info LOCALEDIR:  Locale installation directory.)
	$(info LICENSEDIR: License installation directory.)
	$(info )
	$(info RULES)
	$(info ================================================================================)
	$(info help:      Display this help menu.)
	$(info install:   Install the program and its assets.)
	$(info uninstall: Uninstall the program and its assets.)

install: image2ascii.py po/*.po LICENSE
	$(INSTALL_PROGRAM) -D image2ascii.py '$(INSTALLDIR)/$(PROG)'
	@sed -i '33s/\bNone\b/"$(PROG)"/;34s/\bNone\b/"$(subst /,\/,$(LOCALEDIR))"/' '$(INSTALLDIR)/$(PROG)'
	$(foreach po,$(wildcard po/*.po),$(call generate_mo,$(patsubst po/%.po,%,$(po)));)
	$(INSTALL_DATA) -D -t '$(LICENSEDIR)/$(PROG)' LICENSE

uninstall:
	rm -f '$(INSTALLDIR)/$(PROG)'
	$(foreach po,$(wildcard po/*.po),$(call delete_mo,$(patsubst po/%.po,%,$(po)));)
	rm -rf '$(LICENSEDIR)/$(PROG)'
